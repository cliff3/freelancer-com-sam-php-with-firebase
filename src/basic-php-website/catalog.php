<?php
include("./basic-php-website/inc/data.php");
include("./basic-php-website/inc/functions.php");

$pageTitle = "Full Catalog";
$section = null;

if ($cat_query) {
    if ($cat_query == "books") {
        $pageTitle = "Books";
        $section = "books";
    } else if ($cat_query == "movies") {
        $pageTitle = "Movies";
        $section = "movies";
    } else if ($cat_query == "music") {
        $pageTitle = "Music";
        $section = "music";
    }
}

include("./basic-php-website/inc/header.php"); ?>

    <div class="section catalog page">

        <div class="wrapper">

            <h1><?php
                if ($section != null) {
                    echo "<a href='catalog.php'>Full Catalog</a> &gt; ";
                }
                echo $pageTitle; ?></h1>

            <ul class="items">
                <?php
                $categories = array_category($catalog,$section);
                foreach($categories as $id) {
                    echo get_item_html($id,$catalog[$id]);
                }
                ?>
            </ul>
        </div>

    </div>

<?php include("./basic-php-website/inc/footer.php"); ?>
