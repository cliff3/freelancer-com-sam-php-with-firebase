<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <title>Hello, world!</title>
</head>
<body>

<div class="container">
    <div class="row">
        <div class="col">
            <h1>Phone</h1>
            <form id="phone-form">
                <div class="mb-3">
                    <label class="form-label">Phone</label>
                    <input class="form-control" id="phone" value="+85200000000" />
                </div>

                <div id="recaptcha-container"></div>

                <button id="sign-in-button" class="btn btn-primary">Submit</button>
            </form>
            <form id="token-form">
                <div class="mb-3">
                    <label class="form-label">Token</label>
                    <input class="form-control" id="token" />
                </div>

                <button class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>
</div>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>

<script type="module">
    // Import the functions you need from the SDKs you need
    import { initializeApp } from "https://www.gstatic.com/firebasejs/9.5.0/firebase-app.js";
    import { getAuth, RecaptchaVerifier, signInWithPhoneNumber } from "https://www.gstatic.com/firebasejs/9.5.0/firebase-auth.js";

    document.getElementById('phone-form').addEventListener('submit', (event) => {
        event.preventDefault();
        const phoneNumber = document.querySelector('#phone').value;
        const appVerifier = window.recaptchaVerifier;

        signInWithPhoneNumber(auth, phoneNumber, appVerifier)
            .then((confirmationResult) => {
                // SMS sent. Prompt user to type the code from the message, then sign the
                // user in with confirmationResult.confirm(code).
                window.confirmationResult = confirmationResult;
                alert('SMS sent');
                // ...
            }).catch((error) => {
            window.recaptchaVerifier.render().then(function(widgetId) {
                grecaptcha.reset(widgetId);
            })
            alert(error);
            // Error; SMS not sent
            // ...
        });
    });

    document.getElementById('token-form').addEventListener('submit', (event) => {
        event.preventDefault();

        const token = document.querySelector('#token').value;

        window.confirmationResult.confirm(token).then((result) => {
            alert('User signed in successfully');
        }).catch((error) => {
            alert('User couldn\'t sign in (bad verification code?)');
        });
    });

    // TODO: Add SDKs for Firebase products that you want to use
    // https://firebase.google.com/docs/web/setup#available-libraries

    // Your web app's Firebase configuration
    const firebaseConfig = <?= json_encode($firebase_config) ?>;

    // Initialize Firebase
    const app = initializeApp(firebaseConfig);
    const auth = getAuth();
    // firebase.auth().useDeviceLanguage();

    window.recaptchaVerifier = new RecaptchaVerifier('recaptcha-container', {}, auth);
</script>
</body>
</html>
