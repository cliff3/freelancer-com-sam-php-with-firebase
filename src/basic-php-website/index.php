<?php
include("./basic-php-website/inc/data.php");
include("./basic-php-website/inc/functions.php");

$pageTitle = "Personal Media Library";
$section = null;

include("./basic-php-website/inc/header.php"); ?>
    <div class="section catalog random">
        <div class="wrapper">
            <h2>May we suggest something?</h2>

            <ul class="items">
                <?php
                $random = array_rand($catalog,4);
                foreach($random as $id) {
                    echo get_item_html($id,$catalog[$id]);
                }
                ?>
            </ul>
        </div>
    </div>
<?php include("./basic-php-website/inc/footer.php"); ?>
