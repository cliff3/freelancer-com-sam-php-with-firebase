<?php

function index($data)
{
    if (empty($data['pathParameters']['proxy'])) {
        ob_start();
        include './basic-php-website/index.php';
        $out = ob_get_clean();
        return response($out);
    }

    $url_path = $data['pathParameters']['proxy'];

    if (!file_exists('./basic-php-website/' . $url_path)) {
        return response('File not found');
    }

    $content = [
        'text/plain',
        'text/x-php',
        'text/html',
    ];

    if (!in_array(mime_content_type('./basic-php-website/' . $url_path), $content)) {
        return response('Mime type is not text, html or php. ' . mime_content_type('./basic-php-website/' . $url_path));
    }

    $id_query = null;
    if (isset($data['queryStringParameters']['id'])) {
        $id_query = $data['queryStringParameters']['id'];
    }

    $cat_query = null;
    if (isset($data['queryStringParameters']['cat'])) {
        $cat_query = $data['queryStringParameters']['cat'];
    }

    $firebase_config = [
        "apiKey" => "AIzaSyDqgcNd1hgriK3wqks01H8kKN8H1tijveY",
        "authDomain" => "freelancer-com.firebaseapp.com",
        "projectId" => "freelancer-com",
        "storageBucket" => "freelancer-com.appspot.com",
        "messagingSenderId" => "792410988341",
        "appId" => "1:792410988341:web:603e5d683449a9a22e367c",
    ];

    ob_start();
    include './basic-php-website/' . $url_path;
    $out = ob_get_clean();
    return response($out);
}

function response($body, $content_type = null)
{
    $headers = array(
        "Content-Type" => $content_type ? $content_type : "text/html",
        "Access-Control-Allow-Origin" => "*",
        "Access-Control-Allow-Headers" => "Content-Type",
        "Access-Control-Allow-Methods" => "OPTIONS,POST"
    );

    return json_encode([
        "statusCode" => 200,
        "headers" => $headers,
        "body" => $body,
    ]);
}
